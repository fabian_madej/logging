package fabian.task;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static final Logger log = Logger.getLogger(Main.class.getName());
    public static void main(String[] args) {
        log.log(Level.SEVERE, "severe");
        log.log(Level.WARNING, "warning");
        log.log(Level.INFO, "info");
        log.log(Level.CONFIG, "config");
        log.log(Level.FINE, "fine");
        log.log(Level.FINER, "finer");
        log.log(Level.FINEST, "finest");
        try{
            int i=1/0;
        }catch (ArithmeticException ex){
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }
}
